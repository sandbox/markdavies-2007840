Lexicon Cacheback
=================


ABOUT
-----

This is a simple module to restore caching to lexicon module.


INSTALLATION
------------

1. Copy the lexicon_cacheback folder to your website's sites/all/modules directory.

2. Enable the lexicon_cacheback.module on the Modules page.

3. Go to Configuration > Text formats. For each format that uses lexicon filter you need to:

  a. Click the "configure" link.

  b. Click the "Save configuration" button.


CREDITS
-------

This mini-module was originally made by Mark Davies <mark.davies@codeenigma.com>